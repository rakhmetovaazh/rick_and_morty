import axios from "axios"


const apiKey = 'https://rickandmortyapi.com/api'

const instance = axios.create({
    baseURL: apiKey,
    timeout: 100000,
    headers: {
        "Content-Type": "application/json",
    },
})


export default instance